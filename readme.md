# WORDLE UF3
# NEREA TIRADO CADENAS 

# Introducción 
Primero de todo explicaremos en que consiste la aplicación Wordle, es decir, sus caracteristicas principales.
Este juego consiste en adivinar la palabra en un máximo seis intentos y cinco letras mientras a su vez se va pintando el fondo de las letras dependiendo de su posición. Si su fondo aparece en gris quiere decir que su letra no existe, si aparece en amarillo son las letras que existen, pero no están en su posición indicada y por último tenemos el color verde que informa de la posición o palabra acertada.
Seguidamente, veremos que vamos restando aciertos y si no hemos acertado ninguna vez dentro de ese intervalo la palabra nos aparecerá la palabra que había que adivinar. También si el jugador decide seguir jugando la palabra irá cambiando por cada ronda iniciada.
Por último, al finalizar el juego veremos las estadísticas donde mostrara las jugadas realizadas, victoria, la mejor racha de todas y la distribución en cada momento.

# Implementación del juego 
En esta unidad formativa hemos trabajado con ficheros entonces hemos hecho que el usuario seleccione en cual de los dos idioms que se ofrece quiere jugar y a parti de ahi que le muestre un historial e partidas realizadas acompañadas de sus intentos. 
