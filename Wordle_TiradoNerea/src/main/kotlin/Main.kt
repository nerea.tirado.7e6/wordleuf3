/**
 * @author Nerea Tirado Cadenas
 * @version ultima motidicación el 08/01/2023
 * Principalmente aquí crearemos todas las variables que vamos a ir utilizando a medida que vamos creando nuestra aplicación Wordle.
 *
 * Los intentos los igualamos a 6 porque solo tendremos un máximo de ese intervalo para poder adivinar la palabra.
 */

var listaDePalabras = arrayOf("amigo","antes","anulo","antro","animo","añejo","anime","anglo","angel","anexo","apice","apios","apilo","apelo","apego","aludo","aludi","alzos","aloes","almos","altos","ancho","ambos","anudo","astro","asumo","artes","arcos","arbol","apuro","apure","arabe","aquel","acido","achis","abuso","adios","actuo","acuse","abaco","abeto","alamo","ajeno","algun","aliño","album","adule","aereo","adulo","aguzo","agape","agudo","biche","brote","bacan","bongo","bache","brisa","bolsa","broma","badil","bafle","becas","baile","belga","biela","bajio","blusa","bajos","bizco","blues","brazo","bigas","bufon","bujia","batey","breva","bueno","bicho","baldo","bofar","balin","balon","balto","banyo","banzo","bantu","bañil","baños","bardo","borda","baron","batik","bemol","bidon","betun","borda","botar","brujo","bucal","budin","bucle","burdo","cable","cabos","caldo","calor","calvo","campo","canes","caney","canje","canto","cañon","caños","cardo","carey","cargo","carie","caros","casto","catre","cuaje","cauto","cebar","cedro","cejas","ceibo","celar","celda","cenar","censo","cenit","cesto","cetro","chips","choza","chuzo","cidra","cifra","ciego","cinta","clavo","clero","clima","comer","costa","coxis","crudo","cueva","curva","cutis","danes","daños","darse","datil","datos","decai","debut","decir","dejar","delta","denso","dento","dicha","dicho","dicta","diera","dieta","digno","dijes","diosa","disco","divan","doble","dobla","docil","dogma","doler","domar","donar","dones","dopar","dormi","dotar","drago","dreno","ducha","ducto","duelo","dueña","dulce","duros","ebano","ebrio","echar","ecuas","edito","educa","efuso","egida","elfos","elijo","elite","eluda","emiti","emula","encia","envia","epica","epoca","equis","ergui","ergos","eriza","erigi","espia","esqui","estar","estoy","etano","etica","etimo","etnia","evado","evito","exito","facil","facto","fagot","falco","falso","fango","feria","farol","fasto","favor","fecha","feliz","felpa","femur","fenix","feroz","feudo","fibra","fiare","ficha","fideo","fiera","fijar","filas","filme","final","finca","fingi","fines","firma","flash","flaco","fleco","flora","flota","flema","fluia","flujo","fobia","fogon","force","forja","forma","fosil","foton","frase","friso","freno","fresa","fruta","fuego","fuera","funda","fusil","fusta","gaban","gajos","galan","gales","galon","gasto","gemas","gemir","genio","gesta","giros","glase","gleba","grifo","gozar","golpe","gotas","gozar","grabe","grado","grafo","gramo","grano","grato","grave","greca","grial","grito","grima","gripe","grito","gruñe","grumo","grupo","gruta","guayo","gueto","guiar","guiño","gusto","haber","habil","hacer","harto","hazlo","hebra","hedor","helar","helio","henar","heroe","hervi","hiato","hidra","hielo","higos","hilar","himno","hindu","hiper","hojas","hogar","horas","hotel","hueco","hueso","huevo","huida","humor","hurto","ibero","ideal","ideas","igneo","igual","ilesa","iluso","impar","impio","ingle","infla","insto","inter","iones","infra","irias","istmo","italo","izare","izote","jabon","jalon","jaque","jayan","jedar","jefas","jolin","jopar","joven","joyas","judio","juego","juega","jugar","junio","jumar","junta","junco","justo","juzga","kanji","karts","kefir","kilos","koine","kurda","kurdo","labio","labro","labor","lados","laico","lamer","lance","lando","lanzo","lapos","lapso","lapiz","largo","lares","latex","latir","laton","leuda","laxos","lease","legar","legia","lejos","lento","leñar","leños","lepra","levar","liber","libra","libre","libro","licua","liceo","licor","lider","ligar","light","lijar","limon","limas","linda","linea","lista","listo","litro","lucha","lucid","lucro","luego","lugar","lunar","lunes","luxar","luzco","madre","malos","mando","mango","manos","maori","mapeo","marzo","marte","matiz","mecha","media","medir","medio","megas","melon","manso","menso","menos","menta","mutar","merco","metal","metas","metro","micro","minar","miope","mixto","mofar","mohin","molar","molde","mundo","monje","monja","moral","motin","mover","mucho","mueca","muela","mujer","mundo","nabos","nacar","nacer","negro","nadir","nadie","naipe","nardo","nariz","natio","naves","nebli","necio","negar","nevar","nexos","nicho","niega","nuevo","nieto","nieta","niñez","nivel","nobel","noche","nitro","nogal","norma","norte","notar","nubes","nubil","nuble","nuero","nuevo","ñoras","ñaque","ñecas","ñatos","ñango","ñoqui","ñames","ñajos","ñonga","ñocas","ñique","ñipes","ñinga","oblea","obito","obten","octal","ocupa","odiar","odres","oible","ojala","ojear","ojera","omega","omita","ondas","ondea","opera","opera","optar","orden","oreja","orina","orion","oruga","ostia","otras","ovulo","pacto","padre","palco","polea","panel","pardo","pared","parto","parte","paseo","pasmo","pasto","patin","patio","pecar","pecho","pedal","pedir","pegar","penal","penar","penca","penas","peñon","perno","pesar","pesca","pezon","piano","picar","pecas","picor","pinta","pinza","piñon","pique","pisar","pista","pitar","pixel","pizca","pivot","place","plano","plato","plazo","plano","pluma","podar","poder","poeta","polca","polen","polea","poner","posar","poste","prado","prima","primo","prisa","prosa","pudin","puber","pudor","pugna","pujar","pulir","pulso","pulse","punir","punta","punza","purga","puzle","quark","queco","queda","quedo","queja","quema","quemi","quena","quepo","quera","quien","quien","queso","quijo","quila","quipa","quina","quino","quiño","quiza","quite","quito","recio","radio","radon","rahez","raido","rioja","rajen","raigo","rango","rapte","repta","rapto","rasco","rasgo","raspe","ratos","raudo","rayos","razon","roble","recai","recta","recia","regia","regla","rehen","reino","reloj","renal","remos","renta","resta","reuma","reuso","reves","riego","rifle","rimas","rinde","ruina","rival","ritmo","roble","rompe","ronca","rosca","rumba","rumie","saber","sabio","sabor","sacro","sajon","solde","saldo","salir","salio","salme","salmo","salon","salte","selva","salve","saman","salto","salud","salve","salvo","sanco","santo","saque","satin","sauce","sardo","sauco","sazon","secar","secta","secua","sedal","sedar","segar","segun","segur","senda","senil","señal","señor","sepia","septo","serio","sexto","sigla","siglo","simil","sobre","socia","solar","sonar","soñar","suave","sucio","sudar","suelo","suero","sufre","sumar","super","sutil","tacho","tacon","tacos","tahur","taino","tajos","talco","tales","talon","talud","tamiz","tango","tarde","tardo","tasco","taxon","tauro","techo","tecla","tedio","tejas","telar","telon","temas","temor","tempo","tendi","tenis","tenor","tenso","terco","temor","termo","terso","tirso","tieso","tilde","tilin","timar","timon","teñir","tiple","tiron","tocar","tomar","tunel","topar","toque","torax","toser","trans","trapo","trial","tribu","trina","tripa","triza","tropa","trova","truco","tumba","tupir","turbo","turno","ubres","ubico","ubica","ulema","ultra","umbra","unido","uncir","uncia","ungir","untar","untos","Urano","urdia","urbes","urgio","urjas","urico","urica","usado","usare","usted","utero","uvula","vacio","vagon","video","valer","valor","vamos","vapor","vanos","vario","varon","vasco","vater","vatio","vease","vedar","velar","velon","vengo","venia","venta","venus","veras","venia","verbo","verso","vermu","verti","viaje","vibra","viejo","vieja","vigia","vigor","viral","visco","vital","viudo","visto","viste","vocal","vodka","volar","votar","vuelo","xecas","xinca","xiote","xolas","yacer","yates","yelmo","yergo","yendo","yerba","yedra","yerma","yerna","yerno","yesca","yogui","yogur","yumbo","yunta","yezgo","zafio","zanco","zenda","zetas","zocas","zombi","zueco","zumba","zumbo","zupia","zuñir","zurci","zurdo","zurda","Jacob","Susan","oscar","David","James","Josue","Javier","Aime","Alexa","Anais","Lucas","Adamo","Agnus","Zoila","angel","Antia","Berta","Vilma","Bimba","Celia","Cesar","Nubia","Lucia","Sofia","Lucas","Dario","Mario","Mauro","Frank","Dimas","Edipo","Nicol","Jolie","Julio","Ester","Guido","Linda","Lucio","Marco","Pablo","Ramon","Cadiz","Ibiza","Argel","Cuzco","Andes","Alpes","Sudan","China","Ejion","Judea","Kenia","Moscu","Petra","Nepal","Quito","Saudi","Vegas","Siena","Suiza","ñandu","oveja","cebra","hiena","yegua","cerdo","bagre","lemur","cobra","coqui","okapi","leona","tigre","lince","saino","erizo","ganso","piton","ostra","tucan","mirlo","morsa","potra","corua","mosca","cebra","coati","tenia","chivo")
/**
 * Aqui pondremos el recorrido hacia los archivos con las palabras en cada idioma
 */
var textoPalabrasCast = File("./importPalabras/palabrasCast.txt")
var textoPalabrasIngl = File("./importPalabras/palabrasIngl.txt")

/**
 * Aquí crearemos un menú para que jugador pueda ver las reglas de nuestro juego creado.
 * @author Nerea Tirado Cadenas
 * @version 17/02/2023
 */
fun menu(){
    //creamos un menu para que el jugador sepa como son las reglas del juego
    println("Bienvenido a WORDLE!")
    println("Reglas del juego: ")
    println("------------------------------------------------------")
    println("1. Constara de adivinar la palabra secreta")
    println("2. Consta de tres colores: gris, verde y amarillo")
    println("3. La palabra debera tener 6 letras")
    println("4. Debes de acertarla en un maximmo de seis errores")
    println("------------------------------------------------------")
    var menu = readln()
}
/**
 * Mediante esta función contara cada letra introducida luego pintara de cada color cada letra según su posición
 * @author Nerea Tirado Cadenas
 * @version 17/02/2023
 * @param palabra y palabraUsuario
 */
fun contarLetras(palabra: String, palabraUsuario: String) {
    var posicion: Int
    val contadorDeLetras = mutableMapOf<Char, Int>()
    for (i in palabra) {
        contadorDeLetras.putIfAbsent(i, 0)
        contadorDeLetras[i] = contadorDeLetras[i]!! + 1
    }
    posicion = 0
    //para que te pinte el fondo de cada letra
    for (i in palabraUsuario){
        if (palabra[posicion] == i) {
            print("\u001b[42m$i\u001b[0m")
            contadorDeLetras[i] = contadorDeLetras[i]!! - 1

        }else if (contadorDeLetras[i]!=null && contadorDeLetras[i]!!.toInt()>0){

            print("\u001b[43m$i\u001b[0m")
            contadorDeLetras[i] = contadorDeLetras[i]!! - 1

        }else{
            print(i)
        }
        posicion++
    }
}

/**
 * Esta funcion nueva de menu nos ayuda a saber que idioma a seleccionado el jugador
 * @author Nerea Tirado Cadenas
 * @version 17/02/2023
 */
fun opcMenu2(): Int{
    var opcMenu = """
    Selecciona un idioma 
       # 1. Español 
       # 2. Ingles 
       # 3. Salir 
       """.trimMargin()
    println(opcMenu)
    val selecciona = readln().toInt()
    return selecciona
}

/**
 * COMIENZA LA EJECUCIÓN DEL JUEGO
 * 1. pediremos al jugador que introduzca una palabra
 * 2. iremos restando intentos cada vez que falle y mostrando cuantos nos quedan
 * 3. cuando nos pasemos de cinco letras aquella palabra no servirá
 * @author Nerea Tirado Cadenas
 * @version 17/02/2023
 * @param args
 */
fun main(args: Array<String>) {
    opcMenu2()
    if (idioma==true){
        val file = File("./importPalabras/palabrasCast.txt")
        val diccionario:mutableListOf<String> = mutableListOf<String>()
    } else if (){
        var palabra = listaDePalabras.random()
        println("Has seleccionado castellano: ")
    }
    var posicion = Int
    var intentos = 6
    var palabraUsuario: String = " "
    var sigueJugando: Int = 1

    //mostrar estadisticas 
    val estadisticas = File("./importEstadisticas/estadisticas.txt")
    var partidasJugadas = 0.0
    var partidasGanadas = 0
    var partidasPerdidas = 0
    var porcentajeAciertos = 0.0
    var porcentajeFalso = 0.0
    var rachaActual = 0
    var Racha = 0
    var porcentajesIntentos: Double
    var intentosRealizados = Array(6)
    var intentosParaAdivinar = -1

    menu()
    //println(palabra)

    println("Introduce una palabra: ")

    do {
        println("Intentos restantes: $intentos")
        intentosAdivinaPalabra+=1
        palabraUsuario = readln().toString()
        while (palabraUsuario.length!=5){
            println("la palabra tiene que ser de 5 letras")
            palabraUsuario = readln().toString()
        }
        contarLetras(palabra, palabraUsuario)
        intentos--

        if (intentos==0 || palabra==palabraUsuario){
            println("Quieres seguir jugando? 0 Seguir jugando y 1 Salir")
            sigueJugando = readln().toInt()
            
            println("ACERTASTE!!")
            partidasJugadas++
            partidasGanadas++
            rachaActual++
            intentosRealizados++
            intentosParaAdivinar = 1
            if(rachaActual>=Racha){
                Racha=rachaActual
            }
            if (sigueJugando==0){
                intentos = 6
                palabra = listaDePalabras.random()
            }
        }

    } while (intentos>0 && palabraUsuario!=palabra || sigueJugando != 1){
        esle if(idioma==false){
        val file = File("./importPalabras/palabrasIngl.txt")
        val palabras:MutableList<String> = mutableListOf<String>()
        }
        var palabra = palabras.random()

        println("Has seleccionado el idioma en ingles: ")
    }
    do {
        println("Intentos restantes: $intentos")
        intentosAdivinaPalabra+=1
        palabraUsuario = readln().toString()
        while (palabraUsuario.length!=5){
            println("la palabra tiene que ser de 5 letras")
            palabraUsuario = readln().toString()
        }
        contarLetras(palabra, palabraUsuario)
        intentos--

        if (intentos==0 || palabra==palabraUsuario){
            println("Quieres seguir jugando? 0 Seguir jugando y 1 Salir")
            sigueJugando = readln().toInt()
            
            println("ACERTASTE!!")
            partidasJugadas++
            partidasGanadas++
            rachaActual++
            intentosRealizados++
            intentosParaAdivinar = 1
            if(rachaActual>=Racha){
                Racha=rachaActual
            }
            if (sigueJugando==0){
                intentos = 6
                palabra = listaDePalabras.random()
            }
        }
        estadistcas.writeText("PARTIDAS JUGADAS: ${partidasJugadas.toInt()}")
        estadistcas.appendText("PARTIDAS GANADAS: $partidasGanadas")
        estadistcas.appendText("PARTIDAS PERDIDAS: $partidasPerdidas")
        porcentajeAciertos = (partidasGanadas / partidasJugadas) * 100
        estadistcas.appendText("PORCENTAJE DE PALABRAS RESUELTAS: ${String})
        porcentajeAciertos = (partidasPerdidas / partidasGanadas) * 100
        estadistcas.appendText("PORCENTAJE DE PALABRAS NO RESUELTAS: ${String})
        for (i in 0 until 6) {
            porcentajesIntentos = (intentosRealizados[i] / partidasJugadas) * 100
        }
    }
}